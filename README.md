## React 写的在线渐变生成器

- 完成于 2022-3-20

---

## 功能

### 选择渐变色

### 重置渐变色

### 一键复制渐变色

使用 BOM 中的 `navigator.clipboard.writeText`访问剪切板  
[clipboard-web api 参考](https://developer.mozilla.org/zh-CN/docs/Web/API/Clipboard)

**注意**:

- clipboard API 需要用户授权, 目前只支持 **本地回环** 和 **https** 协议

## 更新

- 新增复制 CSS 代码成功后的文字提示功能

## 未来

---

## <center>学无止境 宁静致远</center>
