import Gradient from './components/Gradient'

function App() {
  console.log(
    [
      '                   _ooOoo_',
      '                  o8888888o',
      '                  88" . "88',
      '                  (| -_- |)',
      '                  O\\  =  /O',
      "               ____/`---'\\____",
      "             .'  \\\\|     |//  `.",
      '            /  \\\\|||  :  |||//  \\',
      '           /  _||||| -:- |||||-  \\',
      '           |   | \\\\\\  -  /// |   |',
      "           | \\_|  ''\\---/''  |   |",
      '           \\  .-\\__  `-`  ___/-. /',
      "         ___`. .'  /--.--\\  `. . __",
      '      ."" \'<  `.___\\_<|>_/___.\'  >\'"".',
      '     | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |',
      '     \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /',
      "======`-.____`-.___\\_____/___.-`____.-'======",
      "                   `=---='",
      '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^',
      '         佛祖保佑       永无BUG',
    ].join('\n')
  )
  console.log('上线时间:  2022年3月   ')
  console.log(`项目地址:  https://gitee.com/KYALEX/react-online-gradient`)
  console.log(`我的博客:  https://akexc.com`)
  return (
    <div className="App" style={{ width: '100vw', height: '100vh' }}>
      <Gradient />
    </div>
  )
}

export default App
