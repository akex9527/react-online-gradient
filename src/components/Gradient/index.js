import React, { useState } from 'react'
import styles from './styles.module.css'
export default function Gradient(props) {
  // react hook 我的理解是对函数组件的增加,让函数组件可以管理自己的状态
  //   useState,接受参数,返回一个状态和管理状态的方法
  const [color1, setColor1] = useState('#00f260')
  const [color2, setColor2] = useState('#0575e6')

  // ref map
  let inputColor1Ref = null
  let inputColor2Ref = null

  function handleColorChange(e) {
    //   ref写法
    // const [firstRefVal, secondRefVal] = [
    //   inputColor1Ref.value,
    //   inputColor2Ref.value,
    // ]
    const { name, value } = e.target
    // console.log(name, value)
    if (name == 'color1') {
      setColor1(value)
    } else setColor2(value)
  }

  function hanleResetBtnClick(e) {
    if (color1 == '#00f260' && color2 == '#0575e6') {
      return false
    }
    setColor1('#00f260')
    setColor2('#0575e6')
    logTip('渐变重置完成~~', { color: '#f10', fontSize: '16px' })
  }

  function logTip(tips, ...arg) {
    console.log(
      `
          %c 温馨提示: %c ${tips}
          `,
      'color:#00f260',
      'color:pink'
    )
  }

  async function hanleCopyBtnClick(e) {
    //navigator.clipboard属性返回 Clipboard 对象，所有操作都通过这个对象进行。
    //如果navigator.clipboard属性返回undefined，就说明当前浏览器不支持这个 API。

    const clipboardObj = navigator.clipboard
    if (clipboardObj) {
      let BgGradientCode = `background: linear-gradient(45deg,${color1},${color2})`
      await navigator.clipboard.writeText(BgGradientCode)
      e.target.innerText = '复制成功'
      let timer = setTimeout(() => {
        e.target.innerText = 'CSS'
        clearTimeout(timer)
      }, 1000)
      logTip('css代码复制成功~~')
    } else {
      alert('您的浏览器版本过低,请更新到最新版本吧~~')
      console.error('不受支持的api')
    }
  }
  return (
    <div
      className={styles.container}
      style={{ background: `linear-gradient(45deg,${color1},${color2})` }}
    >
      <div className={styles.inputGroup}>
        <label htmlFor="" style={{ color: '#fff', fontSize: '1vmax' }}>
          请选择一个颜色
        </label>
        <input
          type="color"
          name="color1"
          className={styles.inputColor}
          value={color1}
          onChange={handleColorChange}
          ref={(el) => (inputColor1Ref = el)}
        />
      </div>
      <div className={styles.inputGroup}>
        <label htmlFor="" style={{ color: '#fff', fontSize: '1vmax' }}>
          请选择一个颜色
        </label>
        <input
          type="color"
          name="color2"
          className={styles.inputColor}
          value={color2}
          onChange={handleColorChange}
          ref={(el) => (inputColor2Ref = el)}
        />
      </div>
      <button className={styles.resetBtn} onClick={hanleResetBtnClick}>
        Reset
      </button>
      <button className={styles.copyBtn} onClick={hanleCopyBtnClick}>
        CSS
      </button>
    </div>
  )
}
